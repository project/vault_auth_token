<?php

namespace Drupal\vault_auth_token\Plugin\VaultAuth;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\key\KeyRepositoryInterface;
use Drupal\vault\Plugin\VaultAuthBase;
use Drupal\vault\Plugin\VaultPluginFormInterface;
use Vault\AuthenticationStrategies\TokenAuthenticationStrategy;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a token-based authentication strategy for the vault client.
 *
 * @VaultAuth(
 *   id = "token",
 *   label = "Vault Token",
 *   description = @Translation("This authentication strategy uses a static token."),
 * )
 */
final class Token extends VaultAuthBase implements ContainerFactoryPluginInterface, VaultPluginFormInterface, ConfigurableInterface {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * The token used to authenticate against Vault.
   *
   * @var string
   */
  protected string $token;

  /**
   * Sets $token property.
   *
   * @param string $token
   *   Authentication token value.
   *
   * @return $this
   *   Current object.
   */
  public function setToken(string $token): static {
    $this->token = $token;
    return $this;
  }

  /**
   * Gets $token property.
   *
   * @return string
   *   Authentication token.
   */
  public function getToken(): string {
    return $this->token;
  }

  /**
   * Constructor a new Token auth plugin instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The translation service.
   * @param \Drupal\key\KeyRepositoryInterface $key_repository
   *   The key repository service.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   On error constructing plugin.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, TranslationInterface $string_translation, KeyRepositoryInterface $key_repository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->setStringTranslation($string_translation);

    if (empty($configuration['token_key_id'])) {
      // Token key is not yet configured.
      $this->setToken('');
      return;
    }

    $key_entity = $key_repository->getKey($configuration['token_key_id']);
    if ($key_entity == NULL) {
      throw new PluginException('Token Key does not exist');
    }

    $token = $key_entity->getKeyValue();
    $this->setToken($token);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('string_translation'),
      $container->get('key.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthenticationStrategy(): TokenAuthenticationStrategy {
    $authStrategy = new TokenAuthenticationStrategy($this->getToken());
    return $authStrategy;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $config = $this->getConfiguration();
    $form['token_key_id'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Vault Token'),
      '#key_filters' => ['type' => 'authentication'],
      '#default_value' => $config['token_key_id'] ?? NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->setConfiguration($form_state->getValues());
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'token_key_id' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    return [];
  }

}
