<?php

namespace Drupal\Tests\vault_auth_token\Unit;

use Drupal\Core\Form\FormState;
use Drupal\key\Entity\Key;
use Drupal\key\KeyRepositoryInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\vault\Plugin\VaultAuthInterface;
use Drupal\vault_auth_token\Plugin\VaultAuth\Token;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Vault\AuthenticationStrategies\TokenAuthenticationStrategy;

/**
 * Tests the Token Auth Plugin.
 *
 * @group vault_auth_token
 * @covers \Drupal\vault_auth_token\Plugin\VaultAuth\Token
 * @codeCoverageIgnore
 */
class AuthTokenTest extends UnitTestCase {

  /**
   * Key Repository service mock.
   *
   * @var \Drupal\key\KeyRepositoryInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $keyRepoMock;

  /**
   * Default Plugin configuration.
   *
   * @var string[]
   */
  protected array $pluginConfig;

  /**
   * The configured VaultAuth Token plugin.
   *
   * @var \Drupal\vault_auth_token\Plugin\VaultAuth\Token
   */
  protected Token $plugin;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $key_entity_mock = $this->createMock(Key::class);
    $key_entity_mock->method('getKeyValue')
      ->willReturn('myroot');

    $return_map = [
      ['vault_token_auth_plugin_ci_key', $key_entity_mock],
      ['invalid_key_id', NULL],
    ];
    $this->keyRepoMock = $this->createMock(KeyRepositoryInterface::class);
    $this->keyRepoMock->method('getKey')
      ->willReturnMap($return_map);

    $this->pluginConfig = [
      'token_key_id' => 'vault_token_auth_plugin_ci_key',
    ];

    $this->plugin = new Token($this->pluginConfig, 'token', [], $this->getStringTranslationStub(), $this->keyRepoMock);

  }

  /**
   * Test the plugin create() method.
   */
  public function testCreate(): void {

    $service_map = [
      [
        'key.repository',
        ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
        $this->keyRepoMock,
      ],
      [
        'string_translation',
        ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
        $this->getStringTranslationStub(),
      ],
    ];

    $container_mock = $this->createMock(ContainerInterface::class);
    $container_mock->method('get')
      ->willReturnMap($service_map);

    $plugin = Token::create($container_mock, $this->pluginConfig, 'approle', []);
    $this->assertInstanceOf(Token::class, $plugin, 'Create returns Token plugin');
  }

  /**
   * Test the Token plugin can be constructed.
   */
  public function testObtainPlugin(): void {
    $this->assertInstanceOf(VaultAuthInterface::class, $this->plugin, 'Token Plugin returned');
    $this->assertEquals(new TokenAuthenticationStrategy('myroot'), $this->plugin->getAuthenticationStrategy(), "Token strategy returned");
  }

  /**
   * Test the Token plugin with no token.
   *
   * Necessary for form first load.
   */
  public function testObtainPluginNoToken(): void {
    $this->plugin = new Token([], 'token', [], $this->getStringTranslationStub(), $this->keyRepoMock);
    $this->assertInstanceOf(VaultAuthInterface::class, $this->plugin, 'Token Plugin returned with no token set');
  }

  /**
   * Test the Token plugin invalid token.
   */
  public function testObtainPluginWithInvalidToken(): void {
    $test_config = [
      'token_key_id' => 'invalid_key_id',
    ];
    $this->expectExceptionMessage('Token Key does not exist');
    $this->plugin = new Token($test_config, 'token', [], $this->getStringTranslationStub(), $this->keyRepoMock);
  }

  /**
   * Test obtain configuration from plugin.
   */
  public function testGetConfiguration(): void {
    $this->assertEquals($this->pluginConfig, $this->plugin->getConfiguration(), 'Plugin returns correct config');
  }

  /**
   * Test obtain the default configuration from plugin.
   */
  public function testDefaultConfiguration(): void {
    $this->assertIsArray($this->plugin->defaultConfiguration(), 'Default Config returns array');
  }

  /**
   * Test set the configuration for plugin.
   */
  public function testSetConfiguration(): void {
    $test_config = [
      'token_key_id' => 'invalid_token_id',
    ];
    $this->plugin = new Token($this->pluginConfig, 'token', [], $this->getStringTranslationStub(), $this->keyRepoMock);
    $this->plugin->setConfiguration($test_config);
    $this->assertEquals($test_config, $this->plugin->getConfiguration());
  }

  /**
   * Test set the configuration for plugin.
   */
  public function testCalculateDependencies(): void {
    $this->assertEquals([], $this->plugin->calculateDependencies());
  }

  /**
   * Test the configuration form build.
   */
  public function testBuildConfigurationForm(): void {
    $form_state = new FormState();
    $this->assertIsArray($this->plugin->buildConfigurationForm([], $form_state));
  }

  /**
   * Test config validation.
   *
   * This is a noop.
   *
   * @doesNotPerformAssertions
   */
  public function testValidateConfigurationForm(): void {
    $form_state = new FormState();
    $form = $this->plugin->buildConfigurationForm([], $form_state);
    $this->plugin->validateConfigurationForm($form, $form_state);
  }

  /**
   * Test config submission.
   */
  public function testSubmitConfigurationForm(): void {
    $form_state = new FormState();
    $form = $this->plugin->buildConfigurationForm([], $form_state);
    $form_state->setValue('token_key_id', 'invalid_token_id');
    $expected_config = [
      'token_key_id' => 'invalid_token_id',
    ];
    $this->plugin->submitConfigurationForm($form, $form_state);
    $this->assertEquals($expected_config, $this->plugin->getConfiguration());
  }

}
