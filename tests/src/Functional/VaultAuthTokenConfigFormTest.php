<?php

namespace Drupal\Tests\vault_auth_token\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests Vault configuration form when using VaultAuthToken.
 *
 * @group vault_auth_token
 * @codeCoverageIgnore
 */
class VaultAuthTokenConfigFormTest extends BrowserTestBase {

  /**
   * A user with administration access.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['vault', 'vault_auth_token', 'key'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $user = $this->drupalCreateUser([
      'administer vault',
    ]);
    if (!$user) {
      $this->fail("Unable to create adminUser");
    }
    $this->adminUser = $user;
    $this->drupalLogin($this->adminUser);

    $key_config = [
      'status' => TRUE,
      'id' => 'vault_ci_test',
      'label' => 'Vault CI Test Key',
      'key_type' => 'authentication',
      'key_provider' => 'config',
      'key_provider_settings' => [
        'key_value' => 'invalid root',
      ],
      'key_input' => 'text_field',
    ];
    $this->config('key.key.vault_ci_test')->setData($key_config)->save(TRUE);
  }

  /**
   * Test the Vault config form.
   */
  public function testVaultConfigurationForm(): void {

    $edit['base_url'] = 'http://vault:8200';
    $edit['plugin_auth'] = 'token';
    $this->drupalGet('admin/config/system/vault');
    // Save the auth method and reload the form.
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->statusCodeEquals(200);
    $edit['plugin_auth_settings[token_key_id]'] = 'vault_ci_test';
    // Submit the form with the token selected.
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->statusCodeEquals(200);
    $vault_config = $this->config('vault.settings')->get();
    assert(is_array($vault_config));
    $this->assertEquals('token', $vault_config['plugin_auth']);
    $this->assertEquals('vault_ci_test', $vault_config['auth_plugin_config']['token_key_id']);
  }

}
